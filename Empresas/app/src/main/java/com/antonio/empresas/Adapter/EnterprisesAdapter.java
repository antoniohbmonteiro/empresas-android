package com.antonio.empresas.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Debug;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.antonio.empresas.Activity.EnterpriseDetailActivity;
import com.antonio.empresas.Model.Enterprise;
import com.antonio.empresas.R;
import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.R.id.empty;
import static android.text.TextUtils.isEmpty;

/**
 * Created by Antonio on 07/08/2017.
 */

public class EnterprisesAdapter extends RecyclerView.Adapter<EnterprisesAdapter.EnterpriseViewHolder> {

    List<Enterprise> enterprises;

    Context context;

    public EnterprisesAdapter(Context context, List<Enterprise> enterprises){
        this.context = context;
        this.enterprises = enterprises;
    }

    @Override
    public EnterpriseViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        EnterpriseViewHolder enterpriseViewHolder = new EnterpriseViewHolder(LayoutInflater.from(context).inflate(R.layout.item_enterprises, null));

        return enterpriseViewHolder;
    }

    @Override
    public void onBindViewHolder(EnterpriseViewHolder enterpriseViewHolder, int i) {
        Enterprise enterprise = enterprises.get(i);

        try{
            enterpriseViewHolder.id = enterprise.getId();
            if(Build.VERSION.SDK_INT >= 21)
                enterpriseViewHolder.logoEnterpriseIV.setTransitionName("logo_enterprise");
            enterpriseViewHolder.nameEnterpriseTV.setText(enterprise.getEnterpriseName());
            enterpriseViewHolder.countryEnterpriseTV.setText(enterprise.getCountry());
            if(enterprise.getEnterpriseType() != null)
                enterpriseViewHolder.typeEnterpriseTV.setText(enterprise.getEnterpriseType().getName());

            if(!isEmpty(enterprise.getPhoto())){
                Glide.with(context).load(enterprise.getPhoto()).into(enterpriseViewHolder.logoEnterpriseIV);
            }

            if(enterprise.getId() == 0) {
                enterpriseViewHolder.logoEnterpriseIV.setVisibility(View.GONE);
                enterpriseViewHolder.typeEnterpriseTV.setVisibility(View.INVISIBLE);
            }
            else {
                enterpriseViewHolder.logoEnterpriseIV.setVisibility(View.VISIBLE);
                enterpriseViewHolder.typeEnterpriseTV.setVisibility(View.VISIBLE);

            }
        }
        catch (Exception ex){
            Log.d("BindViewEnterprise", "Erro BindViewHolder", ex);
        }
    }

    @Override
    public int getItemCount() {
        return enterprises.size();
    }

    public class EnterpriseViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public int id;
        @BindView(R.id.logo_enterprise)
        ImageView logoEnterpriseIV;
        @BindView(R.id.name_enterprise)
        TextView nameEnterpriseTV;
        @BindView(R.id.type_enterprise)
        TextView typeEnterpriseTV;
        @BindView(R.id.country_enterprise)
        TextView countryEnterpriseTV;

        public EnterpriseViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();

            Enterprise enterprise = enterprises.get(position);

            Intent intent = new Intent(context, EnterpriseDetailActivity.class);
            intent.putExtra("id", enterprise.getId());

            if(Build.VERSION.SDK_INT >= 21){
                Pair<View, String> pair1 = Pair.create((View) logoEnterpriseIV, logoEnterpriseIV.getTransitionName());
                ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation((Activity) context, pair1);
                context.startActivity(intent, optionsCompat.toBundle());
            }
            else{
                context.startActivity(intent);
            }
        }
    }
}
