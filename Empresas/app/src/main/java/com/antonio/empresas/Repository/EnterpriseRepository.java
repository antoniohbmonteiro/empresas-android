package com.antonio.empresas.Repository;

import com.antonio.empresas.Model.ResponseModel.EnterpriseResponse;
import com.antonio.empresas.Model.ResponseModel.EnterprisesListModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Antonio on 08/08/2017.
 */

public interface EnterpriseRepository {

    @GET("enterprises")
    Call<EnterprisesListModel> getEnterpriseIndex(@Query("name") String name,
                                                  @Query("enterprise_types") String type);


    @GET("enterprises/{id}")
    Call<EnterpriseResponse> get(@Path("id") int id);

    @GET("enterprises")
    Call<EnterpriseResponse> getAll();
}
