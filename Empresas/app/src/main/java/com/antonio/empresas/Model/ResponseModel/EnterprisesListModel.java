package com.antonio.empresas.Model.ResponseModel;

import com.antonio.empresas.Model.Enterprise;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Antonio on 08/08/2017.
 */

public class EnterprisesListModel {

    @SerializedName("enterprises")
    @Expose
    private List<Enterprise> enterpriseList;

    public List<Enterprise> getEnterpriseList() {
        return enterpriseList;
    }

    public void setEnterpriseList(List<Enterprise> enterpriseList) {
        this.enterpriseList = enterpriseList;
    }
}
