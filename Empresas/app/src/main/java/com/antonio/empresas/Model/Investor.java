package com.antonio.empresas.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Antonio on 29/07/2017.
 */

public class Investor {
    // Não sei quals valores podem vir null, colocando todos na versao Objeto

    @Expose
    @SerializedName("id")
    private Integer id;

    @Expose
    @SerializedName("investor_name")
    private String name;

    @Expose
    @SerializedName("id")
    private String email;

    @Expose
    @SerializedName("city")
    private String city;

    @Expose
    @SerializedName("country")
    private String country;

    @Expose
    @SerializedName("balance")
    private Integer balance;

    @Expose
    @SerializedName("photo")
    private String photo;

    @Expose
    @SerializedName("first_access")
    private Boolean firstAccess;

    @Expose
    @SerializedName("super_angel")
    private Boolean superAngel;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Boolean getFirstAccess() {
        return firstAccess;
    }

    public void setFirstAccess(Boolean firstAccess) {
        this.firstAccess = firstAccess;
    }

    public Boolean getSuperAngel() {
        return superAngel;
    }

    public void setSuperAngel(Boolean superAngel) {
        this.superAngel = superAngel;
    }
}
