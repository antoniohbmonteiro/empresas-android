package com.antonio.empresas.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.antonio.empresas.BuildConfig;
import com.antonio.empresas.R;
import com.antonio.empresas.Repository.LoginRepository;
import com.antonio.empresas.Session;
import com.antonio.empresas.Utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Header;
import retrofit2.http.Headers;

import static android.text.TextUtils.isEmpty;

// Todo: Criar padrão de projeto MVP
public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.email_editText)
    EditText email_editText;
    @BindView(R.id.senha_editText)
    EditText senha_editText;
    @BindView(R.id.entrar_button)
    Button entrar_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

//        if(BuildConfig.DEBUG){
//            email_editText.setText("testeapple@ioasys.com.br");
//            senha_editText.setText("12341234");
//        }

        if(!isEmpty(Session.sharedInstance().getAccessToken(this)) || !isEmpty(Session.sharedInstance().getClient(this)) || !isEmpty(Session.sharedInstance().getUid(this)) ){
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
        }
    }

    //  Todo: Criar erro do EditText
    @OnClick(R.id.entrar_button)
    public void entrar_buttonOnClick() {
        if (validateEditText()) {
            String emailDigitado = email_editText.getText().toString();
            String senhaDigitada = senha_editText.getText().toString();

            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("email", emailDigitado);
            hashMap.put("password", senhaDigitada);

            try {

                Utils.retrofit(LoginActivity.this).create(LoginRepository.class).login(hashMap).enqueue(new Callback<HashMap<String, Object>>() {
                    @Override
                    public void onResponse(Call<HashMap<String, Object>> call, Response<HashMap<String, Object>> response) {

                        okhttp3.Headers allHeaders = response.headers();
                        String accessToken = allHeaders.get("access-token");
                        String client = allHeaders.get("client");
                        String uid = allHeaders.get("uid");

                        if(!isEmpty(accessToken) || !isEmpty(uid) || !isEmpty(client)){
                            Session.sharedInstance().setAcessToken(LoginActivity.this, accessToken);
                            Session.sharedInstance().setClient(LoginActivity.this, client);
                            Session.sharedInstance().setUid(LoginActivity.this, uid);

                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            startActivity(intent);
                        }

                    }

                    @Override
                    public void onFailure(Call<HashMap<String, Object>> call, Throwable t) {

                    }
                });
            } catch (Exception ex) {
                Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

//    private boolean validateLogin(){
//
//    }

    private boolean validateEditText() {
        boolean validate = true;
        String email = email_editText.getText().toString();
        String password = senha_editText.getText().toString();

        if (isEmpty(email)) {
            email_editText.setError("E-mail não pode ser vazio!");
            validate = false;
        }
        else if(!isEmailValid(email)){
            email_editText.setError("Precisa ser um e-mail válido");
            validate = false;
        }
        if (isEmpty(password)) {
            senha_editText.setError("Senha não pode ser vazia!");
            validate = false;
        }

        return validate;
    }

    boolean isEmailValid(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
}
