package com.antonio.empresas.Model.ResponseModel;

import com.antonio.empresas.Model.Enterprise;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Antonio on 08/08/2017.
 */

public class EnterpriseResponse {

    @SerializedName("enterprise")
    @Expose
    private Enterprise enterprise;

    @SerializedName("success")
    @Expose
    private boolean success;

    public Enterprise getEnterprise() {
        return enterprise;
    }

    public void setEnterprise(Enterprise enterprise) {
        this.enterprise = enterprise;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
