package com.antonio.empresas.Repository;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by Antonio on 29/07/2017.
 */

public interface LoginRepository {

    @POST("users/auth/sign_in/")
    Call<HashMap<String, Object>> login(@Body HashMap<String, String> body);

}
