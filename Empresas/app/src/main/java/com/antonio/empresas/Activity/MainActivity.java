package com.antonio.empresas.Activity;

import android.graphics.Color;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.antonio.empresas.Adapter.EnterprisesAdapter;
import com.antonio.empresas.Model.Enterprise;
import com.antonio.empresas.Model.ResponseModel.EnterprisesListModel;
import com.antonio.empresas.R;
import com.antonio.empresas.Repository.EnterpriseRepository;
import com.antonio.empresas.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.searchView)
    SearchView searchView;

    @BindView(R.id.logo_toolbar)
    ImageView logoToolbar;

    @BindView(R.id.text_clique_buscar)
    TextView textCliqueBuscar;

    @BindView(R.id.appbar)
    AppBarLayout appbar;

    List<Enterprise> enterpriseList;

    EnterprisesAdapter enterprisesAdapter;
    LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        enterpriseList = new ArrayList<>();

        enterprisesAdapter = new EnterprisesAdapter(this, enterpriseList);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(enterprisesAdapter);


        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    logoToolbar.setVisibility(View.GONE);
                    textCliqueBuscar.setVisibility(View.GONE);
                    enterpriseList.clear();
                    enterprisesAdapter.notifyDataSetChanged();
                    recyclerView.setVisibility(View.VISIBLE);
                } else {
                    logoToolbar.setVisibility(View.VISIBLE);
                    textCliqueBuscar.setVisibility(View.VISIBLE);
                    enterpriseList.clear();
                    enterprisesAdapter.notifyDataSetChanged();
                    recyclerView.setVisibility(View.GONE);
                    searchView.setIconified(true);
                    appbar.setExpanded(true);
                }
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(   String s) {
//                Toast.makeText(MainActivity.this, "Submit", Toast.LENGTH_SHORT).show();


                getEnterprises(s);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
//                Toast.makeText(MainActivity.this, "Text Changed", Toast.LENGTH_SHORT).show();

                //ToDo : pesquisar somente depois de um tempo (300ms por exemplo) assim que parar de digitar
                // coloquei

//                if(s.length() > 0){
//
//                }

                getEnterprises(s);

                return true;
            }
        });
    }

    private void getEnterprises(String name){
        // passando null em enterprise_type
        Utils.retrofit(this).create(EnterpriseRepository.class).getEnterpriseIndex(name, null).enqueue(new Callback<EnterprisesListModel>() {
            @Override
            public void onResponse(Call<EnterprisesListModel> call, Response<EnterprisesListModel> response) {
                EnterprisesListModel resposta = response.body();

                enterpriseList.clear();

                if(resposta != null && resposta.getEnterpriseList() != null){

                    if(resposta.getEnterpriseList().size() > 0) {
                        enterpriseList.addAll(resposta.getEnterpriseList());
                    }
                    else{
                        Enterprise enterprise = new Enterprise();
                        enterprise.setEnterpriseName("Não gerou resultados!");
                        enterpriseList.add(enterprise);
                    }
                }

                enterprisesAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<EnterprisesListModel> call, Throwable t) {

            }
        });
    }


//    @OnClick(R.id.searchView)
//    public void searchViewOnClick(){
//        logoToolbar.setVisibility(View.GONE);
//        textCliqueBuscar.setVisibility(View.GONE);
//    }
}
