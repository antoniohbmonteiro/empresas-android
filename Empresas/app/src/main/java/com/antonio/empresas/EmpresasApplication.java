package com.antonio.empresas;

import android.app.Application;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Antonio on 29/07/2017.
 */

public class EmpresasApplication extends Application {


    @Override
    public void onCreate() {
        super.onCreate();

        Session.init(EmpresasApplication.this);



    }
}
