package com.antonio.empresas.Activity;

import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.transition.TransitionInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.antonio.empresas.Model.Enterprise;
import com.antonio.empresas.Model.ResponseModel.EnterpriseResponse;
import com.antonio.empresas.R;
import com.antonio.empresas.Repository.EnterpriseRepository;
import com.antonio.empresas.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EnterpriseDetailActivity extends AppCompatActivity {

    @BindView(R.id.toolbarDetail)
    Toolbar toolbarDetail;

    @BindView(R.id.logoDetail)
    ImageView logoDetail;

    @BindView(R.id.enterpriseDescription)
    TextView enterpriseDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enterprise_detail);

        ButterKnife.bind(this);

        toolbarDetail.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if(Build.VERSION.SDK_INT >= 21) {
            getWindow().setSharedElementEnterTransition(TransitionInflater.from(this).inflateTransition(R.transition.shred_element_transation));
            logoDetail.setTransitionName("logo_enterprise");
        }

        int id = getIntent().getIntExtra("id", 0);

        getEnterprise(id);
    }

    private void getEnterprise(int id) {
        Utils.retrofit(this).create(EnterpriseRepository.class).get(id).enqueue(new Callback<EnterpriseResponse>() {
            @Override
            public void onResponse(Call<EnterpriseResponse> call, Response<EnterpriseResponse> response) {
                if(response.body() != null){
                    Enterprise enterprise = response.body().getEnterprise();

                    if(enterprise != null){
                        toolbarDetail.setTitle(enterprise.getEnterpriseName());
                        enterpriseDescription.setText(enterprise.getDescription());
                    }

                }

            }

            @Override
            public void onFailure(Call<EnterpriseResponse> call, Throwable t) {

            }
        });
    }
}
