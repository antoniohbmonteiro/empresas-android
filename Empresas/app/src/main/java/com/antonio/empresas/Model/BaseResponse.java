package com.antonio.empresas.Model;

import java.util.List;

/**
 * Created by Antonio on 29/07/2017.
 */

public class BaseResponse {

    private boolean success;
    private List<String> erros;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<String> getErros() {
        return erros;
    }

    public void setErros(List<String> erros) {
        this.erros = erros;
    }
}
