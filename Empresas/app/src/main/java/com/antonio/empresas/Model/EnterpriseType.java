package com.antonio.empresas.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Antonio on 07/08/2017.
 */

public class EnterpriseType {

    @SerializedName("id")
    @Expose
    private int Id;

    @SerializedName("enterprise_type_name")
    @Expose
    private String Name;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }
}
