package com.antonio.empresas;

import android.content.Context;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Antonio on 29/07/2017.
 */

public class Utils {

    public static String endpoint = "http://54.94.179.135:8090/api/v1/";

    public static Retrofit retrofit(final Context context) {

        final OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.connectTimeout(2, TimeUnit.MINUTES);
        httpClient.readTimeout(2, TimeUnit.MINUTES);


        try{
            httpClient.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();

                    String accessToken = Session.sharedInstance().getAccessToken(context);
                    String client = Session.sharedInstance().getClient(context);
                    String uid = Session.sharedInstance().getUid(context);
//                    String accessToken = "iJqZWkYgksqZaaZkNifyqA";
//                    String client = "Br7HiorB--F9_Anf6ar-Hw";
//                    String uid = "testeapple@ioasys.com.br";


                    Request request = original.newBuilder()
                            .header("access-token", accessToken)
                            .header("client", client)
                            .header("uid", uid)
                            .build();

                    return chain.proceed(request);
                }
            });
        }
        catch (Exception ex){
            Toast.makeText(context, "", Toast.LENGTH_SHORT).show();
        }

        OkHttpClient cliente = httpClient.build();


        Gson gsonBuilder = new GsonBuilder()
                .setLenient()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(endpoint)
                .client(cliente)
                .addConverterFactory(GsonConverterFactory.create(gsonBuilder))
                .build();

        return retrofit;
    }

}
