package com.antonio.empresas;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

/**
 * Created by Antonio on 29/07/2017.
 */

public class Session {

    private static Session instance = null;

    Context context;

    private Session(Context context) {
        this.context = context;
    }

    public static void init(Context context) {
        try{
            if (instance == null) {
                instance = new Session(context);
            }
        }
        catch (Exception ex){
            Toast.makeText(context, "", Toast.LENGTH_SHORT).show();
        }
    }

    public static Session sharedInstance() {
        return instance;
    }


    public String getAccessToken(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences("Empresas", Context.MODE_PRIVATE );
        return sharedPreferences.getString("access-token", "");
    }

    public void setAcessToken(Context context, String accessToken){
        SharedPreferences sharedPref = context.getSharedPreferences("Empresas", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("access-token", accessToken);
        editor.apply();
    }
    public String getClient(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences("Empresas", Context.MODE_PRIVATE );
        return sharedPreferences.getString("client", "");
    }

    public void setClient(Context context, String client){
        SharedPreferences sharedPref = context.getSharedPreferences("Empresas", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("client", client);
        editor.apply();
    }
    public String getUid(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences("Empresas", Context.MODE_PRIVATE );
        return sharedPreferences.getString("uid", "");
    }

    public void setUid(Context context, String uid){
        SharedPreferences sharedPref = context.getSharedPreferences("Empresas", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("uid", uid);
        editor.apply();
    }

}
